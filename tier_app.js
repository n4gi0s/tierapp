var request = require('./async_request');

class TierApp {
    constructor() {
        this.agent = 'TierRelease/3.3.10 (app.tier.sharing; build:3.3.10.0; iOS 13.3.0) Alamofire/4.9.1';
        this.apikey = 'bpEUTJEBTf74oGRWxaIcW7aeZMzDDODe1yBoSxi2';
    }
    async getVehicles() {
        let {
            res,
            body
        } = await request.get({
            url: 'https://platform.tier-services.io/vehicle',
            headers: {
                'user-agent': this.agent,
                'x-api-key': this.apikey
            },
            qs: {
                radius: 1 //is needed but why?
            }
        });
        return JSON.parse(body).data;
    }
}
module.exports = TierApp;