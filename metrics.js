var express = require('express');
const client = require('prom-client');
var router = express.Router();
const register = client.register;
const Gauge = client.Gauge;
var TierApp = require('./tier_app');

var gauges = {
    batteryLevel: new Gauge({
        name: 'tier_scooter_batteryLevel',
        help: 'batteryLevel',
        labelNames: ['id']
    }),
    lat: new Gauge({
        name: 'tier_scooter_lat',
        help: 'lat',
        labelNames: ['id']
    }),
    lng: new Gauge({
        name: 'tier_scooter_lng',
        help: 'lng',
        labelNames: ['id']
    }),
    active: new Gauge({
        name: 'tier_scooter_active',
        help: 'active',
        labelNames: ['id']
    }),
    rentable: new Gauge({
        name: 'tier_scooter_rentable',
        help: 'rentable',
        labelNames: ['id']
    }),
    maxSpeed: new Gauge({
        name: 'tier_scooter_maxSpeed',
        help: 'maxSpeed',
        labelNames: ['id']
    }),
    code: new Gauge({
        name: 'tier_scooter_code',
        help: 'code',
        labelNames: ['id']
    }),
    lastLocationUpdate: new Gauge({
        name: 'tier_scooter_lastLocationUpdate',
        help: 'lastLocationUpdate',
        labelNames: ['id']
    }),
    lastStateChange: new Gauge({
        name: 'tier_scooter_lastStateChange',
        help: 'lastStateChange',
        labelNames: ['id']
    }),
    zone: new Gauge({
        name: 'tier_scooter_zone',
        help: 'zone',
        labelNames: ['zone']
    }),
    vendor: new Gauge({
        name: 'tier_scooter_vendor',
        help: 'vendor',
        labelNames: ['vendor']
    })
};

var tier = new TierApp();
router.get('/', async (req, res) => {
    register.resetMetrics(); //remove old data
    let vehicles = await tier.getVehicles();
    for (let vehicle of vehicles) {
        gauges.batteryLevel.set({
            id: vehicle.id
        }, vehicle.batteryLevel);
        gauges.lat.set({
            id: vehicle.id
        }, vehicle.lat);
        gauges.lng.set({
            id: vehicle.id
        }, vehicle.lng);
        gauges.active.set({
            id: vehicle.id
        }, vehicle.state == 'ACTIVE' ? 1 : 0);
        gauges.rentable.set({
            id: vehicle.id
        }, vehicle.isRentable ? 1 : 0);
        gauges.maxSpeed.set({
            id: vehicle.id
        }, vehicle.maxSpeed);
        gauges.code.set({
            id: vehicle.id
        }, vehicle.code);
        gauges.lastLocationUpdate.set({
            id: vehicle.id
        }, new Date(vehicle.lastLocationUpdate).getTime());
        gauges.lastStateChange.set({
            id: vehicle.id
        }, new Date(vehicle.lastStateChange).getTime());
        gauges.zone.inc({
            zone: vehicle.zoneId
        });
        gauges.vendor.inc({
            vendor: vehicle.iotVendor
        });
    }
    res.set('Content-Type', register.contentType);
    res.end(register.metrics());
});

module.exports = router;