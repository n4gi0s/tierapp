var request = require('request');



module.exports = {
    get: async (options) => {
        return new Promise((resolve, reject) => {
            request.get(options, (err, res, body) => {
                if (err) return reject(err);
                if (res.statusCode != 200) return reject(new Error(res.statusCode + ' ' + res.statusMessage));
                resolve({
                    res,
                    body
                });
            });
        });
    },
    post: async (options) => {
        return new Promise((resolve, reject) => {
            request.post(options, (err, res, body) => {
                if (err) return reject(err);
                if (res.statusCode != 200) return reject(new Error(res.statusCode + ' ' + res.statusMessage));
                resolve({
                    res,
                    body
                });
            });
        });
    },
    put: async (options) => {
        return new Promise((resolve, reject) => {
            request.put(options, (err, res, body) => {
                if (err) return reject(err);
                if (res.statusCode != 200) return reject(new Error(res.statusCode + ' ' + res.statusMessage));
                resolve({
                    res,
                    body
                });
            });
        });
    }
};