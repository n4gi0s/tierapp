var TierAPP = require('./tier_app');
var express = require('express');

var app = express();
var port = process.env.PORT || 3030;
app.use('/metrics', require('./metrics.js'));
app.listen(port, '0.0.0.0');
console.log('*metrics started on port ' + port);